//
//  MasterViewController.m
//  GithubApiDemo
//
//  Created by James Cash on 30-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];

    [self loadGithubReposForUser:@"jamesnvc"];
}

- (void)loadGithubReposForUser:(NSString*)userName
{
    // Make the url for the github api request to view repositories for the given user
    NSURL *ghUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.github.com/users/%@/repos", userName]];
    NSLog(@"Creating task");
    // create the task to request the URL (by sending a GET request)
    NSURLSessionTask *task =
    [[NSURLSession sharedSession]
     dataTaskWithURL:ghUrl
     // When the response is recieved, run this block, passing it the result of the request
     // this code is *not* being executed now - it is being "saved for later" to be run by the URLSessionTask once it's finished downloading
     completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
         NSLog(@"Task completed");
         if (error) {
             NSLog(@"An error occured: %@", error.localizedDescription);
         }
         NSLog(@"Got a response of type %@", response.MIMEType);
         NSError *err;
         // We want to take our data object, which contains a string, representing JSON data, which we happen to know is an array of dictionaries with string keys
         // and turn it into an array of dictionaries we can access
         // so we use the built-in NSJSONSerialization methods
         // Note that *we* need to know what the format of this JSON is - if it actually was a dictionary of things, not an array of dictionaries, this would just break and the system wouldn't be able to help us
         NSArray *repos = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
         // Pretty important to check that we were able to correctly parse out our JSON into a data structure
         if (err) {
             NSLog(@"Error deserializing json: %@", err.localizedDescription);
         }
         // Loop over all the dictionaries of repo information and extract out the repo names
         NSMutableArray *repoNames = [[NSMutableArray alloc] init];
         for (NSDictionary *repoDict in repos) {
             NSLog(@"Repo name: %@", repoDict[@"name"]);
             [repoNames addObject:repoDict[@"name"]];
         }
         self.objects = repoNames;
         // IN this case, we don't need to do this
         // but in general, if you want to change the UI from a block, you probably will need to send it to the "main" queue, since all UI changes must happen on the main thread
         dispatch_async(dispatch_get_main_queue(), ^{
             [self.tableView reloadData];
         });
     }];
    NSLog(@"Starting task");
    // By default, the task we created to download isn't running yet, so we need to start it
    [task resume];
}


- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSDate *object = self.objects[indexPath.row];
    cell.textLabel.text = [object description];
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


@end
